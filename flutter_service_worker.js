'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  ".git/COMMIT_EDITMSG": "377ad60d660aa7d316534a8bd57399ee",
".git/config": "86c73cb329e6757311a81abd31548c6e",
".git/description": "a0a7c3fff21f2aea3cfa1d0316dd816c",
".git/HEAD": "4cf2d64e44205fe628ddd534e1151b58",
".git/hooks/applypatch-msg.sample": "ce562e08d8098926a3862fc6e7905199",
".git/hooks/commit-msg.sample": "579a3c1e12a1e74a98169175fb913012",
".git/hooks/fsmonitor-watchman.sample": "a0b2633a2c8e97501610bd3f73da66fc",
".git/hooks/post-update.sample": "2b7ea5cee3c49ff53d41e00785eb974c",
".git/hooks/pre-applypatch.sample": "054f9ffb8bfe04a599751cc757226dda",
".git/hooks/pre-commit.sample": "305eadbbcd6f6d2567e033ad12aabbc4",
".git/hooks/pre-merge-commit.sample": "39cb268e2a85d436b9eb6f47614c3cbc",
".git/hooks/pre-push.sample": "2c642152299a94e05ea26eae11993b13",
".git/hooks/pre-rebase.sample": "56e45f2bcbc8226d2b4200f7c46371bf",
".git/hooks/pre-receive.sample": "2ad18ec82c20af7b5926ed9cea6aeedd",
".git/hooks/prepare-commit-msg.sample": "2b5c047bdb474555e1787db32b2d2fc5",
".git/hooks/push-to-checkout.sample": "c7ab00c7784efeadad3ae9b228d4b4db",
".git/hooks/update.sample": "647ae13c682f7827c22f5fc08a03674e",
".git/index": "7565aeaae4c128caf1cd0f6f70cf5ab0",
".git/info/exclude": "036208b4a1ab4a235d75c181e685e5a3",
".git/logs/HEAD": "ec093d1228eee1dbb2a113bc21bf51af",
".git/logs/refs/heads/master": "ec093d1228eee1dbb2a113bc21bf51af",
".git/logs/refs/remotes/origin/master": "74a6c7447a1c90929e81735a0ccc5dad",
".git/objects/02/29b6693f2c2239d0dfd28178eed8bc3a23b04c": "117e8b1759802c5b8f4220cb964e1d5d",
".git/objects/05/85cb33351486ee3ef41f40408f6f554f9aa17b": "391daab173bc918899bbb2bae786ebe9",
".git/objects/0a/78fb2244743acc5773d72ccf54f90bde2bc6f8": "fc3bd56368853436677db4f0020d9751",
".git/objects/0c/8c88d12fa647350ef9f49068a45cbe3e34b3a3": "e848d012658f28fcf84a29c851d5ef9d",
".git/objects/0d/374ebd08561b6384599164fe4977e32ace96e6": "68f41c74001d756e97d13fedc9fa9191",
".git/objects/12/3fd655178c82644783f110f12db3ed2127c4de": "6fe165a01032b22176e0144432b56c4d",
".git/objects/12/4e80d103663f970b1a7825916681d31756d5fd": "ae6112209a16303346a269ac06453b30",
".git/objects/12/5c284dc2bbed823ed6389e50cc75a8a1ef2351": "9ab0bcea773dc4fd6ef989aef459b28a",
".git/objects/2e/d9f905b0284bfbb42d8801eb88ed6788d14704": "96f319bc2cc20e4736f2f4ae7881d2ef",
".git/objects/37/7ead7488ba04497de54363ee4e4436edf85774": "d08a584d3f7b4fb2a65c159d695456ff",
".git/objects/3b/7e9f9a31554d95e267ac42aef59a89a65b46ef": "1e266311417795d904e95b21487c4405",
".git/objects/3d/4ceb8433f20c020787c532f97da156ee60b82e": "20195b9fa632dd67309fdee08e7e33be",
".git/objects/3d/f5dcd904107a01f88edda5f10bd202bb15d660": "ced2eff1f0e46307d9f14b72d902d2d8",
".git/objects/41/5c059c8094b888b0159fdedfd4e3cb08a8028e": "86914685ccd40e82a7fe5b70459fb9f7",
".git/objects/47/dec78a2d61921ce9f7709055ebea0b58329266": "e6d2795032f16d43c44c9b051a20d871",
".git/objects/4b/a42792e21cb0527a68af7c3067abae405af0ae": "0b6070d414b5f0d4c6814a9cc8381e24",
".git/objects/4b/ce11f832f26ee264d95b6790237601cf5a6088": "d9243641979aa81a886b057ab392745a",
".git/objects/55/919b0b32f21410d9755ffbe64aeb2d07650744": "ffb88073b04c5f9454f1e7f08edc733c",
".git/objects/62/819751618b348ecad4606f0cc031c93b10bb51": "fe8a116c6e3d398e87efc43cb5d83398",
".git/objects/6a/562868741911ffc3a5345f3c094a71eb0d073d": "ad3237b80e6dc65be0e2000c6c0a445f",
".git/objects/6c/384ab33a5080d226305423c8d777031c8a25e7": "ca57f1921bc07835130627c41085dd96",
".git/objects/73/a5ca9e6ea457a8e87e3e88047b9268f15bb75b": "437e873491b95548ba60fecc00fa7933",
".git/objects/75/d23bd9477a4cb2b41bfadaf692f2fc80a5d64a": "1740376433844346397e0bef07fb3310",
".git/objects/76/22e0270997a92f544a222a285d48d100d0a44f": "54bbcc7a558e1bfc8a762b43fe0c0fa3",
".git/objects/79/ba7ea0836b93b3f178067bcd0a0945dbc26b3f": "f3e31aec622d6cf63f619aa3a6023103",
".git/objects/7e/040a30c2537fac15a53a35fd6345c90fbce8b9": "e92fa630ea949d439dec27f6b37ce05c",
".git/objects/88/cfd48dff1169879ba46840804b412fe02fefd6": "e42aaae6a4cbfbc9f6326f1fa9e3380c",
".git/objects/8a/aa46ac1ae21512746f852a42ba87e4165dfdd1": "1d8820d345e38b30de033aa4b5a23e7b",
".git/objects/8c/99266130a89547b4344f47e08aacad473b14e0": "41375232ceba14f47b99f9d83708cb79",
".git/objects/91/9ab9d681ca62e17f98cabd43493b6f350fd3b7": "e7b0c8d83f01106fe1bf0112f6efae7b",
".git/objects/9d/d1912f55b5f5163241723e9e3ad72564a2482a": "80f6f3f0783c1f8428208cabe3627527",
".git/objects/9e/3074a14e007c9847af1e98f35aa4faa8252b9d": "6fd1bd117b0f60866ee212353836b48d",
".git/objects/9f/c0d7efcaae3b11211b55eedf6ca4ebf6f03480": "ffdf4184c3e41bf160243f49e644d65a",
".git/objects/a1/3837a12450aceaa5c8e807c32e781831d67a8f": "bfe4910ea01eb3d69e9520c3b42a0adf",
".git/objects/a2/5e233a9a4b9258af5ba6a70f8dacbc996133f4": "56abe04ca84fe0bb75414562ca3cce55",
".git/objects/aa/d07f26c08763ee4aa0bb594b4a8c561abef403": "bf90829405400fa49aee2c7540a5f4b6",
".git/objects/aa/f9775aa89c03137dbf48721cdc05191703de5e": "25802b31f0bee36b4e1a7a67835f99f0",
".git/objects/ab/0e98497a51ead7821d1da35a24968ff314e50f": "557c35fe3928eb2af403d1b3926bb9ba",
".git/objects/ac/56718631b6e82df25b7e2c8ec544bbddc58d53": "e7d15e617982fd1b34f8ac3306015916",
".git/objects/b0/84a99458aaf6bb96f80720316bab3cf39bbf0c": "cbc7b9997b9425fc159d15a9ce595a25",
".git/objects/b7/49bfef07473333cf1dd31e9eed89862a5d52aa": "36b4020dca303986cad10924774fb5dc",
".git/objects/b9/2a0d854da9a8f73216c4a0ef07a0f0a44e4373": "f62d1eb7f51165e2a6d2ef1921f976f3",
".git/objects/c8/9501c49998baf455b6479e8beea617350d7b32": "fbfb252cd3d2336d2e915629c621c379",
".git/objects/c9/c61d295a743c7fccf4fbcb0a531ad11b0daa41": "fa83dc5b7cca10c2d1f3dd8dacb0be00",
".git/objects/d2/c27b7e7b1dcd6b583df34412a11b93af8ab31c": "2f32586171103127ec54cf131b6b78a5",
".git/objects/d4/474b6d9c2f1fb78871003922a8f34098677234": "e59e06ff1ec0be3b66217d70e8609fb9",
".git/objects/d4/725e0458dbcb7ab3dca9c0ca59c6a830aba1d4": "71466d0f0441bb40332c1ec38c0b7d4c",
".git/objects/d6/9c56691fbdb0b7efa65097c7cc1edac12a6d3e": "868ce37a3a78b0606713733248a2f579",
".git/objects/d9/ab838eec5c8c8cc9c8cc6c9e1f3c6b61e96cce": "83a23bde314d47f0fb98d9ab2b5b01ae",
".git/objects/dc/f853f2b9e34fcdc45ec17c0a5061b45817f321": "6bd78e626216a75105965a8e7ac8877b",
".git/objects/e1/c82be3fc888153b81f38d3515c61356316f909": "c94fadc7042a30995626a3ff7699a72a",
".git/objects/e3/20147985ba167dec3d8ac28322cb006e82fd96": "8933f2b6d689c3c98fde1c8b5301575d",
".git/objects/e5/951dfb943474a56e611d9923405cd06c2dd28d": "c6fa51103d8db5478e1a43a661f6c68d",
".git/objects/e6/0fb6f36c943208aa70b340c001b5cba0084b8e": "66a3717e5df00a7e7115bd8882b8d25d",
".git/objects/eb/9b4d76e525556d5d89141648c724331630325d": "37c0954235cbe27c4d93e74fe9a578ef",
".git/objects/ef/7298703e9a39fc0aa3a5941ab84044f3458580": "8112063a52cb032eeef74e7956abd908",
".git/objects/f0/b2c6169b43b037459aa5653c52047f8b935795": "1be7973ea14d40bf2c7c4ece792c757a",
".git/objects/f1/67462eab86d113c90bfac043fa8a08d058b13d": "f7502cec54866e1d6a295546bff4bf63",
".git/objects/fd/55db405b6e6effe2518c68696d2bf39f659dcc": "2c54ce0cb64f61e67125f279d1bd30ea",
".git/objects/ff/de0662f34f1ed7d5b4aeaf64a985d27efcc2a2": "169752dbab17cd9d1ee5362813803202",
".git/refs/heads/master": "82477e7817a7f06eff5371a2f0f6092c",
".git/refs/remotes/origin/master": "82477e7817a7f06eff5371a2f0f6092c",
"assets/AssetManifest.json": "fa1cd9192555a600e70726e04291a802",
"assets/assets/fonts/caviar/CaviarDreams.ttf": "fd2d0a4d699ed411275cb14ef35dec7d",
"assets/assets/fonts/caviar/Caviar_Dreams_Bold.ttf": "c32e6db748bed398b70c105d50b52ec7",
"assets/assets/fonts/TTRounds.ttf": "955efeb4359c8287847029b692534118",
"assets/assets/fonts/TTRoundsRegular.ttf": "058f0e17fc5a7a81a0a5ce29eacb44ef",
"assets/assets/images/Asset1.png": "26192bc9b83fec30358fe2650970990c",
"assets/assets/images/Asset2.png": "0ced9d288eb5a088af8c2bb552139284",
"assets/assets/images/cocacola.png": "2a71f584a56d2889e63d9781e7c60578",
"assets/assets/images/dashboard.png": "9465ca3ca51775510108a0e93b029742",
"assets/assets/images/fb.png": "87510654950b544e0aa98c9ae4a89bd6",
"assets/assets/images/foods.png": "f234f0ba921a8aeea935e8a65f2f065d",
"assets/assets/images/google.png": "baa58fbc1529cfdee811fd2cd6ad9a14",
"assets/assets/images/halal.png": "b2f92023074ae8a934f20a905085c17b",
"assets/assets/images/illustration1.png": "07d124578e6482f4987fa83aa7d72d67",
"assets/assets/images/Illustration2.png": "04b3f0c5b8b28c3a5b1440f0bf023696",
"assets/assets/images/illustration3.png": "c4232b12a609e84636e17e2f0245f4fe",
"assets/assets/images/Illustrator.png": "e5aa8436bf30187a8ebc3cf8019292f8",
"assets/assets/images/insta.png": "dd1d0987ed42ace21c3c5039f44e3804",
"assets/assets/images/isocert.png": "899c282838c2ef675df2bc4a2cee6ee6",
"assets/assets/images/isocert1.png": "54c6167c0f004672614fbc53b09120d5",
"assets/assets/images/linkedin.png": "524ddcf957551a4b6282cf9f28825b6d",
"assets/assets/images/linkin.png": "9cc20dfa8891eb757a4d781a25a992ef",
"assets/assets/images/samsung.png": "fc7b329944af591fc4beb52b537647ea",
"assets/assets/images/setalogo.png": "9ea6f43f25c3f574a3d2e01edc12059a",
"assets/assets/images/tiktok.png": "24fa477251be9374214f85bd9b06c14c",
"assets/assets/images/vector.png": "64d7bc95f8dfee122bc908558b8f5e8c",
"assets/assets/images/vector1.png": "ba783f6c8540ebe7c4c621d15b6b7102",
"assets/FontManifest.json": "9fae21e1d832a96cf3aaf825f8bb3888",
"assets/fonts/MaterialIcons-Regular.otf": "e7069dfd19b331be16bed984668fe080",
"assets/NOTICES": "31541af2eb915dc1e0f8450b243b1656",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "6d342eb68f170c97609e9da345464e5e",
"canvaskit/canvaskit.js": "97937cb4c2c2073c968525a3e08c86a3",
"canvaskit/canvaskit.wasm": "3de12d898ec208a5f31362cc00f09b9e",
"canvaskit/profiling/canvaskit.js": "c21852696bc1cc82e8894d851c01921a",
"canvaskit/profiling/canvaskit.wasm": "371bc4e204443b0d5e774d64a046eb99",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"flutter.js": "a85fcf6324d3c4d3ae3be1ae4931e9c5",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"icons/Icon-maskable-192.png": "c457ef57daa1d16f64b27b786ec2ea3c",
"icons/Icon-maskable-512.png": "301a7604d45b3e739efc881eb04896ea",
"index.html": "2d61f187d49841ab17b6ab694d4090a0",
"/": "2d61f187d49841ab17b6ab694d4090a0",
"main.dart.js": "2ffd17f1ebb23a3bcf75889c223b3679",
"manifest.json": "273b2b806e45e7129e4c58a07084cbf6",
"version.json": "00e85d1727564b6e7a57a5c8e7cecda4"
};

// The application shell files that are downloaded before a service worker can
// start.
const CORE = [
  "main.dart.js",
"index.html",
"assets/AssetManifest.json",
"assets/FontManifest.json"];
// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});

// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});

// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache only if the resource was successfully fetched.
        return response || fetch(event.request).then((response) => {
          if (response && Boolean(response.ok)) {
            cache.put(event.request, response.clone());
          }
          return response;
        });
      })
    })
  );
});

self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});

// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}

// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
